DEMO SHELL
---

**ECHO** <br>

>**Exercice 1** <br>
>Ecrivez un script shell qui affiche "Hello World from the MOON!"  <br>

**VARIABLE** <br>

>**EXERCICE 2** <br>
>Modifier le script de l'exercice 1 afin d'inclure une variable <br>
>Cette variable devra contenir le message "Hello World from the MOON!" <br>

>**EXERCICE 3**  <br>
>Enregistrer la sortie de la commande "hostname" dans une variable <br>
>et afficher "Ce script s'exécute sur '_'" <br>
>"_" est la sortie de la commande hostname <br>

**IF-ELSE** <br>

>**EXERCICE 4**  <br>
>Ecrivez un script shell qui vérifie l'existence d'un fichier config.yaml
>Si le fichier existe, affichez "Config found"
>Sinon, affichez "Config not found" et créer le fichier 
>Si le fichier existe, vérifiez que vous avez les permissions de l'editer
>Si vous pouvez l'éditer, afficher "Vous avez les droits d'écriture sur ce fichier"
>Sinon, afficher "Vous n'avez pas les droits d'écriture sur ce fichier"

**READ INPUT** <br>

>**EXERCICE 5**  <br>
>Ecrivez un script shell qui demande à l'utilisateur le nom d'un fichier
>Reportez qu'il s'agit soit d'un fichier, d'un repertoire ou d'un autre type de fichier

**OPERATEUR** <br>

>**EXERCICE 6** <br>
>Créer un script qui demande à l'utilisateur de saisir une note et qui affiche un message en fonction de cette note : 
>« très bien » si la note est entre 16 et 20 ; 
>« bien » lorsqu'elle est entre 14 et 16 ;
>« assez bien » si la note est entre 12 et 14 ; 
>« moyen » si la note est entre 10 et 12 ; 
>« insuffisant » si la note est inférieur à 10.

**PARAMETRE** <br>

>**EXERCICE 7**  <br>
>Modifiez le script de l'exercice 5 de sorte qu'il accepte le fichier en paramètre 
>plutôt qu'en entrée utilisateur. <br>

Note
>echo "all params: $*"<br>
>echo "number of params: $#"<br>
>echo "user $1"<br>
>echo "group $2"<br>
> $1 : First Parameter<br>
> $2 : 2nd Parameter<br>
> $? : Derniere commande <br>
> $* : Tous les parametres<br>
> $# : Nombre de caracteres<br>

**BOUCLE FOR** <br>

>**EXERCICE 8**  <br>
>Ecrivez un script shell qui affiche "Linux", "Git", "Docker", "Kubernetes", "Jenkins", "DevOps"
>sur des lignes différentes. Ecrivez le en le moins de ligne possible  

```sh
for param in $*
do 
    if [ -d "$param" ] 
    then
        echo "executing scripts in the config folder"
        ls -l "$param"
    fi 
    echo $param
done
```

<br>

**BOUCLE WHILE** <br>

>**EXERCICE 9**  <br>
>Ecrivez un programme qui demande à l'utilisateur un score jusqu'à ce qu'il saisisse "q"
>Faites l'addition des valeurs saisies et afficher le score final 

**FONCTION** <br>

>**EXERCICE 10**  <br>
>Resolvez l'exercice 9 avec une fonction

```sh
# Declare function
function score_sum {
  sum = 0
	while true
	 do 
		read -p "enter a score" score
	
	  if [ "$score" == "q" ]
	  then
	   break
	  fi
	
	  sum=$(($sum+$score))
	  echo "total score: $sum"
	 done
}

# Invoke function
score_sum
```

>**EXERCICE 11**  <br>
> Ecrivez une fonction qui crée le fichier en premier argument et attribue des permissions d'execution au
> owner si le second argument est true

```sh
function create_file() {
  file_name=$1
  is_shell_script=$2
  touch $file_name
  echo "file $file_name created" 

  if [ "$is_shell_script" = true ]
  then
		chmod u+x $file_name
		echo "added execute permission"
	fi
}

# Invoke with diff params
create_file test.txt
create_file myfile.yaml
create_file myscript.sh
```

>**EXERCICE 12**  <br>
> Ecrivez une fonction qui retourne la somme du premier et deuxieme argument 

```sh
function sum() {
	total=$(($1+$2))
  return $total
}

sum 2 10
result=$?

echo "sum of 2 and 10 is $result"
```



